Docker环境部署
=========

# System requirements(系统需求)
* Ubuntu 14.04+ (x86-64)

# Software requirements(软件需求)
* rancher/server 1.6.2
* rancher/agernt 1.2.7

# Deployment(部署介绍)
* Registry(发布镜像代理环境)
```
sudo bash docker_registry_install
```

* Server(服务环境)
```
sudo bash docker_server_install
```

* Development(开发环境)
```
sudo bash docker_develop_install
```
